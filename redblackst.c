/*
 * MAC0323 Estruturas de Dados e Algoritmo II
 *
 * Tabela de simbolos implementada atraves de uma BST rubro-negra
 *
 *     https://algs4.cs.princeton.edu/33balanced/RedBlackBST.java.html
 *
 * As chaves e valores desta implementação são mais ou menos
 * genéricos
 */

/* interface para o uso da funcao deste módulo */
#include "redblackst.h"

#include <stdlib.h>  /* free() */
#include <string.h>  /* memcpy() */
#include "util.h"    /* emalloc(), ecalloc() */

#undef DEBUG
#ifdef DEBUG
#include <stdio.h>   /* printf(): para debug */
#endif

/*
 * CONSTANTES
 */
#define RED   TRUE
#define BLACK FALSE

/*----------------------------------------------------------*/
/*
 * Estrutura Básica da Tabela de Símbolos:
 *
 * implementação com árvore rubro-negra
 */
typedef struct node Node;
struct redBlackST {
    Node* root;
    int (*cmp) (const void* a, const void* b);
    int iterator;
};

/*----------------------------------------------------------*/
/*
 * Estrutura de um nó da árvore
 *
 */

/* tipo genérico p guardar key/val */
typedef struct generic Generic;
struct generic {
    void* data;
    size_t size;
};

struct node {
    Generic key;
    Generic val;
    Node *left, *right;
    Bool color;
    int size;
};

/*------------------------------------------------------------*/
/*
 *  Protótipos de funções administrativas.
 *
 *  Entre essa funções estão isRed(), rotateLeft(), rotateRight(),
 *  flipColors(), moveRedLeft(), moveRedRight() e balance().
 *
 *  Não deixe de implmentar as funções chamadas pela função
 *  check(): isBST(), isSizeConsistent(), isRankConsistent(),
 *  is23(), isBalanced().
 *
 */

/*---------------------------------------------------------------*/
static Bool
isBST(RedBlackST st);

/*---------------------------------------------------------------*/
static Bool
isSizeConsistent(RedBlackST st);

/*---------------------------------------------------------------*/
static Bool
isRankConsistent(RedBlackST st);

/*---------------------------------------------------------------*/
static Bool
is23(RedBlackST st);

/*---------------------------------------------------------------*/
static Bool
isBalanced(RedBlackST st);

/* Protótipo das minhas funções adm */
/* Genéricos */
Generic createGeneric(const void* data, size_t nData);
void updateGeneric(Generic* p, const void* data, size_t nData);
void freeGeneric(Generic* p);
void* getCopyGeneric(Generic p);

/* Nós */
Node* createNode(const void* key, size_t nKey, const void* val, size_t nVal, Bool color, int size);
void freeNode(Node* p);
int sizeNode(Node* p);

Bool isRed(Node* p);

Node* minNode(Node* p);
Node* maxNode(Node* p);

int heightNode(Node* p);
int rankNode(Node* x, const void* key, RedBlackST st);

Node* deleteMinNode(Node* h);
Node* deleteMaxNode(Node* h);
Node* deleteNode(Node* h, const void* key, RedBlackST st);

Node* nodePut(Node* h, const void* key, size_t nKey, const void* val, size_t nVal, RedBlackST st);

Node* selectNode(Node* x, int k);

Bool isBSTNode(RedBlackST st, Node* x, const void* min, const void* max);
Bool is23Node(Node* x, RedBlackST st);
Bool isBalancedNode(Node* x, int black);
Bool isSizeConsistentNode(Node* x);

/* BST */
Node* rotateRight(Node* h);
Node* rotateLeft(Node* h);

void flipColors(Node* h);

Node* moveRedLeft(Node* h);
Node* moveRedRight(Node* h);

Node* balance(Node* h);


/*-----------------------------------------------------------*/
/*
 *  initST(COMPAR)
 *
 *  RECEBE uma função COMPAR() para comparar chaves.
 *  RETORNA (referência/ponteiro para) uma tabela de símbolos vazia.
 *
 *  É esperado que COMPAR() tenha o seguinte comportamento:
 *
 *      COMPAR(key1, key2) retorna um inteiro < 0 se key1 <  key2
 *      COMPAR(key1, key2) retorna 0              se key1 == key2
 *      COMPAR(key1, key2) retorna um inteiro > 0 se key1 >  key2
 *
 *  TODAS OS OPERAÇÕES da ST criada utilizam a COMPAR() para comparar
 *  chaves.
 *
 */
RedBlackST
initST(int (*compar)(const void *key1, const void *key2))
{
    RedBlackST st = emalloc(sizeof(struct redBlackST));
    st->root = NULL;
    st->cmp = compar;
    st->iterator = 0;
    return st;
}

/*-----------------------------------------------------------*/
/*
 *  freeST(ST)
 *
 *  RECEBE uma RedBlackST  ST e devolve ao sistema toda a memoria
 *  utilizada por ST.
 *
 */
void
freeST(RedBlackST st)
{
    freeNode(st->root);
    free(st);
}


/*------------------------------------------------------------*/
/*
 * OPERAÇÕES USUAIS: put(), get(), contains(), delete(),
 * size() e isEmpty().
 */

/*-----------------------------------------------------------*/
/*
 *  put(ST, KEY, NKEY, VAL, NVAL)
 *
 *  RECEBE a tabela de símbolos ST e um par KEY-VAL e procura a KEY na ST.
 *
 *     - se VAL é NULL, a entrada da chave KEY é removida da ST
 *
 *     - se KEY nao e' encontrada: o par KEY-VAL é inserido na ST
 *
 *     - se KEY e' encontra: o valor correspondente é atualizado
 *
 *  NKEY é o número de bytes de KEY e NVAL é o número de bytes de NVAL.
 *
 *  Para criar uma copia/clone de KEY é usado o seu número de bytes NKEY.
 *  Para criar uma copia/clode de VAL é usado o seu número de bytes NVAL.
 *
 */

void
put(RedBlackST st, const void *key, size_t sizeKey, const void *val, size_t sizeVal)
{
    if (key == NULL) return;
    if (val == NULL) { delete(st, key); return; }

    st->root = nodePut(st->root, key, sizeKey, val, sizeVal, st);
    st->root->color = BLACK;
}

Node* nodePut(Node* h, const void* key, size_t nKey,
const void* val, size_t nVal, RedBlackST st) {
    int cmp;
    if (h == NULL) return createNode(key, nKey, val, nVal, RED, 1);

    cmp = st->cmp(key, h->key.data);
    if (cmp < 0) h->left = nodePut(h->left, key, nKey, val, nVal, st);
    else if (cmp > 0) h->right = nodePut(h->right, key, nKey, val, nVal, st);
    else  updateGeneric(&(h->val), val, nVal);

    if (isRed(h->right) && !isRed(h->left)) h = rotateLeft(h);
    if (isRed(h->left) && isRed(h->left->left)) h = rotateRight(h);
    if (isRed(h->left) && isRed(h->right)) flipColors(h);
    h->size = sizeNode(h->left) + sizeNode(h->right) + 1;

    return h;
}


/*-----------------------------------------------------------*/
/*
 *  get(ST, KEY)
 *
 *  RECEBE uma tabela de símbolos ST e uma chave KEY.
 *
 *     - se KEY está em ST, RETORNA NULL;
 *
 *     - se KEY não está em ST, RETORNA uma cópia/clone do valor
 *       associado a KEY.
 *
 */
void *
get(RedBlackST st, const void *key)
{
    Node* p; int cmp;
    if (key == NULL) return NULL;

    p = st->root;
    while(p != NULL){
        cmp = st->cmp(key, p->key.data);
        if (cmp < 0) p = p->left;
        else if (cmp > 0) p = p->right;
        else return getCopyGeneric(p->val);
    }
    return NULL;
}


/*-----------------------------------------------------------*/
/*
 *  CONTAINS(ST, KEY)
 *
 *  RECEBE uma tabela de símbolos ST e uma chave KEY.
 *
 *  RETORNA TRUE se KEY está na ST e FALSE em caso contrário.
 *
 */
Bool
contains(RedBlackST st, const void *key)
{
    void* r; Bool ans;
    r = get(st,key);
    ans = r != NULL;
    if (ans) free(r);
    return ans;
}

/*-----------------------------------------------------------*/
/*
 *  DELETE(ST, KEY)
 *
 *  RECEBE uma tabela de símbolos ST e uma chave KEY.
 *
 *  Se KEY está em ST, remove a entrada correspondente a KEY.
 *  Se KEY não está em ST, faz nada.
 *
 */
void
delete(RedBlackST st, const void *key)
{
    if (key == NULL) return;
    if (!contains(st, key)) return;

    if (!isRed(st->root->left) && !isRed(st->root->right))
        st->root->color = RED;

    st->root = deleteNode(st->root, key, st);
    if (!isEmpty(st)) st->root->color = BLACK;
}

Node* deleteNode(Node* h, const void* key, RedBlackST st) {
    Node* x;
    if (st->cmp(key, h->key.data) < 0) {
        if (!isRed(h->left) && !isRed(h->left->left))
            h = moveRedLeft(h);
        h->left = deleteNode(h->left, key, st);
    }
    else {
        if (isRed(h->left))
            h = rotateRight(h);

        if (st->cmp(key, h->key.data) == 0 && (h->right == NULL)) {
            freeNode(h);
            return NULL;
        }

        if (!isRed(h->right) && !isRed(h->right->left))
            h = moveRedRight(h);

        if (st->cmp(key,h->key.data) == 0) {
            x = minNode(h->right);
            updateGeneric(&h->key, x->key.data, x->key.size);
            updateGeneric(&h->val, x->val.data, x->val.size);
            h->right = deleteMinNode(h->right);
        }

        else h->right = deleteNode(h->right, key, st);
    }
    return balance(h);
}


/*-----------------------------------------------------------*/
/*
 *  SIZE(ST)
 *
 *  RECEBE uma tabela de símbolos ST.
 *
 *  RETORNA o número de itens (= pares chave-valor) na ST.
 *
 */
int
size(RedBlackST st)
{
    return sizeNode(st->root);
}


/*-----------------------------------------------------------*/
/*
 *  ISEMPTY(ST, KEY)
 *
 *  RECEBE uma tabela de símbolos ST.
 *
 *  RETORNA TRUE se ST está vazia e FALSE em caso contrário.
 *
 */
Bool
isEmpty(RedBlackST st)
{
    return st->root == NULL;
}

/*------------------------------------------------------------*/
/*
 * OPERAÇÕES PARA TABELAS DE SÍMBOLOS ORDENADAS:
 * min(), max(), rank(), select(), deleteMin() e deleteMax().
 */

/*-----------------------------------------------------------*/
/*
 *  MIN(ST)
 *
 *  RECEBE uma tabela de símbolos ST e RETORNA uma cópia/clone
 *  da menor chave na tabela.
 *
 *  Se ST está vazia RETORNA NULL.
 *
 */
void *
min(RedBlackST st)
{
    if (isEmpty(st)) return NULL;
    return getCopyGeneric(minNode(st->root)->key);
}

Node* minNode(Node* p) { return (p->left == NULL) ? p : minNode(p->left); }


/*-----------------------------------------------------------*/
/*
 *  MAX(ST)
 *
 *  RECEBE uma tabela de símbolos ST e RETORNA uma cópia/clone
 *  da maior chave na tabela.
 *
 *  Se ST está vazia RETORNA NULL.
 *
 */
void *
max(RedBlackST st)
{
    if (isEmpty(st)) return NULL;
    return getCopyGeneric(maxNode(st->root)->key);
}

Node* maxNode(Node* p) { return (p->right == NULL) ? p : maxNode(p->right); }

/*-----------------------------------------------------------*/
/*
 *  RANK(ST, KEY)
 *
 *  RECEBE uma tabela de símbolos ST e uma chave KEY.
 *  RETORNA o número de chaves em ST menores que KEY.
 *
 *  Se ST está vazia RETORNA NULL.
 *
 */
int
rank(RedBlackST st, const void *key)
{
    if (key == NULL) return 0;
    return rankNode(st->root, key, st);
}

int rankNode(Node* x, const void *key, RedBlackST st) {
    int cmp;
    if (x == NULL) return 0;
    cmp = st->cmp(key, x->key.data);
    if (cmp < 0) return rankNode(x->left, key, st);
    if (cmp > 0) return 1 + rankNode(x->right, key, st) + sizeNode(x->left);
    else return sizeNode(x->left);
}

/*-----------------------------------------------------------*/
/*
 *  SELECT(ST, K)
 *
 *  RECEBE uma tabela de símbolos ST e um inteiro K >= 0.
 *  RETORNA a (K+1)-ésima menor chave da tabela ST.
 *
 *  Se ST não tem K+1 elementos RETORNA NULL.
 *
 */
void *
select(RedBlackST st, int k)
{
    Node* x;
    if (k < 0 || k >= size(st))
        return NULL;
    x = selectNode(st->root, k);
    return getCopyGeneric(x->key);
}

Node* selectNode(Node* x, int k) {
    int t = sizeNode(x->left);
    if (t > k) return selectNode(x->left, k);
    else if (t < k) return selectNode(x->right, k-t-1);
    return x;
}


/*-----------------------------------------------------------*/
/*
 *  deleteMIN(ST)
 *
 *  RECEBE uma tabela de símbolos ST e remove a entrada correspondente
 *  à menor chave.
 *
 *  Se ST está vazia, faz nada.
 *
 */
void
deleteMin(RedBlackST st)
{
    if (isEmpty(st)) return;
    if (!isRed(st->root->left) && !isRed(st->root->right))
        st->root->color = RED;
    st->root = deleteMinNode(st->root);
    if (!isEmpty(st)) st->root->color = BLACK;
}

Node* deleteMinNode(Node* h) {
    if (h->left == NULL) { freeNode(h); return NULL;}

    if (!isRed(h->left) && !isRed(h->left->left))
        h = moveRedLeft(h);

    h->left = deleteMinNode(h->left);
    return balance(h);
}


/*-----------------------------------------------------------*/
/*
 *  deleteMAX(ST)
 *
 *  RECEBE uma tabela de símbolos ST e remove a entrada correspondente
 *  à maior chave.
 *
 *  Se ST está vazia, faz nada.
 *
 */
void
deleteMax(RedBlackST st)
{
    if (isEmpty(st)) return;

    if (!isRed(st->root->left) && !isRed(st->root->left))
        st->root->color = RED;

    st->root = deleteMaxNode(st->root);
    if (!isEmpty(st)) st->root->color = BLACK;
}

Node* deleteMaxNode(Node* h) {
    if (isRed(h->left))
        h = rotateRight(h);

    if (h->right == NULL) {
        freeNode(h);
        return NULL;
    }

    if (!isRed(h->right) && !isRed(h->right->left))
        h = moveRedRight(h);

    h->right = deleteMaxNode(h->right);

    return balance(h);
}


/*-----------------------------------------------------------*/
/*
 *  KEYS(ST, INIT)
 *
 *  RECEBE uma tabela de símbolos ST e um Bool INIT.
 *
 *  Se INIT é TRUE, KEYS() RETORNA uma cópia/clone da menor chave na ST.
 *  Se INIT é FALSE, KEYS() RETORNA a chave sucessora da última chave retornada.
 *  Se ST está vazia ou não há sucessora da última chave retornada, KEYS() retorna NULL.
 *
 *  Se entre duas chamadas de KEYS() a ST é alterada, o comportamento é
 *  indefinido.
 *
 */
void *
keys(RedBlackST st, Bool init)
{
    if (init) st->iterator = 0;
    else st->iterator++;
    return select(st, st->iterator);
}



/*------------------------------------------------------------*/
/*
 * Funções administrativas
 */

/***************************************************************************
 *  Utility functions.
 ***************************************************************************/

/*
 * HEIGHT(ST)
 *
 * RECEBE uma RedBlackST e RETORNA a sua altura.
 * Uma BST com apenas um nó tem altura zero.
 *
 */
int
height(RedBlackST st)
{
    return heightNode(st->root);
}

int heightNode(Node* p) {
    int l,r;
    if (p == NULL) return -1;
    l = heightNode(p->left);
    r = heightNode(p->right);
    return 1 + (l > r) ? l : r;
}


/***************************************************************************
 *  Check integrity of red-black tree data structure.
 ***************************************************************************/

/*
 * CHECK(ST)
 *
 * RECEBE uma RedBlackST ST e RETORNA TRUE se não encontrar algum
 * problema de ordem ou estrutural. Em caso contrário RETORNA
 * FALSE.
 *
 */
Bool
check(RedBlackST st)
{
    if (!isBST(st))            ERROR("check(): not in symmetric order");
    if (!isSizeConsistent(st)) ERROR("check(): subtree counts not consistent");
    if (!isRankConsistent(st)) ERROR("check(): ranks not consistent");
    if (!is23(st))             ERROR("check(): not a 2-3 tree");
    if (!isBalanced(st))       ERROR("check(): not balanced");
    return isBST(st) && isSizeConsistent(st) && isRankConsistent(st) && is23(st) && isBalanced(st);
}


/*
 * ISBST(ST)
 *
 * RECEBE uma RedBlackST ST.
 * RETORNA TRUE se a árvore é uma BST.
 *
 */
static Bool
isBST(RedBlackST st)
{
    return isBSTNode(st, st->root, NULL, NULL);
}

Bool isBSTNode(RedBlackST st, Node* x, const void* min, const void* max) {
    if (x == NULL) return TRUE;
    if (min != NULL && st->cmp(x->key.data, min) <= 0) return FALSE;
    if (max != NULL && st->cmp(x->key.data, max) >= 0) return FALSE;
    return isBSTNode(st, x->left, min, x->key.data) && isBSTNode(st, x->right, x->key.data, max);
}


/*
 *  ISSIZECONSISTENT(ST)
 *
 *  RECEBE uma RedBlackST ST e RETORNA TRUE se para cada nó h
 *  vale que size(h) = 1 + size(h->left) + size(h->right) e
 *  FALSE em caso contrário.
 */
static Bool
isSizeConsistent(RedBlackST st) { return isSizeConsistentNode(st->root); }
Bool isSizeConsistentNode(Node* x) {
    if (x == NULL) return TRUE;
    if (x->size != (sizeNode(x->left) + sizeNode(x->right) + 1)) return FALSE;
    return isSizeConsistentNode(x->left) && isSizeConsistentNode(x->right);
}

/*
 *  ISRANKCONSISTENT(ST)
 *
 *  RECEBE uma RedBlackST ST e RETORNA TRUE se seus rank() e
 *  select() são consistentes.
 */
/* check that ranks are consistent */
static Bool
isRankConsistent(RedBlackST st)
{
    void* key, *key2; int i;
    for (i = 0; i < size(st); i++) {
        key = select(st, i);
        if (i != rank(st, key)) return FALSE;
        free(key);
    }

    key = keys(st, TRUE);
    while (key != NULL) {
        key2 = select(st,rank(st,key));
        if (st->cmp(key, key2) != 0) return FALSE;
        free(key); free(key2);
        key = keys(st, FALSE);
    }
    return TRUE;
}

/*
 *  IS23(ST)
 *
 *  RECEBE uma RedBlackST ST e RETORNA FALSE se há algum link RED
 *  para a direta ou se ha dois links para esquerda seguidos RED
 *  Em caso contrário RETORNA TRUE (= a ST representa uma árvore 2-3).
 */
static Bool
is23(RedBlackST st) { return is23Node(st->root, st); }

Bool is23Node(Node* x, RedBlackST st) {
    if (x == NULL) return TRUE;
    if (isRed(x->right)) return FALSE;
    if (x != st->root && isRed(x) && isRed(x->left))
        return FALSE;
    return is23Node(x->left, st) && is23Node(x->right, st);
}


/*
 *  ISBALANCED(ST)
 *
 *  RECEBE uma RedBlackST ST e RETORNA TRUE se st satisfaz
 *  balanceamento negro perfeiro.
 */
static Bool
isBalanced(RedBlackST st)
{
    int black; Node* x;
    black = 0;
    x = st->root;
    while (x != NULL) {
        if (!isRed(x)) black++;
        x = x->left;
    }
    return isBalancedNode(st->root, black);
}

Bool isBalancedNode(Node* x, int black) {
    if (x == NULL) { return black == 0;}
    if (!isRed(x)) black--;
    return isBalancedNode(x->left, black) && isBalancedNode(x->right, black);
}

/* Funções e itens adminstrativos */
/*
    GENERATE(DATA, NDATA)
    Recebe um valor e seu tamanho, retorna cópia
    como uma estrutura genérica
*/
Generic createGeneric(const void* data, size_t nData) {
    Generic t;
    t.data = emalloc(nData);
    memcpy(t.data, data, nData);
    t.size = nData;
    return t;
}

/*
    UPDATEDATA(GENERIC*, DATA, NDATA)
    Atualiza valor da estrutura genérica p para
    data com tamanho nData
*/
void updateGeneric(Generic* p, const void* data, size_t nData) {
    free(p->data);
    p->data = emalloc(nData);
    memcpy(p->data, data, nData);
    p->size = nData;
}

/*
    FREEGENERIC(GENERIC*)
    Wrapper para liberar memória de um
    genérico.
*/
void freeGeneric(Generic* p) {
    free(p->data);
}

/*
    GETCOPYGENERIC(GENERIC)
    Retorna cópia do valor de um
    genérico
*/
void* getCopyGeneric(Generic p) {
    void* copy = emalloc(p.size);
    memcpy(copy, p.data, p.size);
    return copy;
}

/*
    FUNÇÕES ADM. NODE
*/
/*
    CREATENODE(KEY, NKEY, VAL, NVAL, COLOR, SIZE)
    Cria um novo nó, com chave e valor, cor
    e tamanho passados
*/
Node* createNode(const void* key, size_t nKey,
const void* val, size_t nVal, Bool color, int size) {
    Node* p = emalloc(sizeof(Node));

    p->key = createGeneric(key, nKey);
    p->val = createGeneric(val, nVal);
    p->color = color;
    p->size = size;
    p->left = NULL;
    p->right = NULL;

    return p;
}

/*
    FREENODE(NODE)
    Libera um nó e todos os seus filhos
*/
void freeNode(Node* p) {
    if (p == NULL) return;

    freeNode(p->left);
    freeNode(p->right);
    freeGeneric(&p->key);
    freeGeneric(&p->val);
    free(p);
}

/* SIZENODE(NODE) : retorna tamanho da aŕvore que o nó representa*/
int sizeNode(Node* p) { if (p == NULL) return 0; else return p->size; }

/* Retorna se o nó é vermelho */
Bool isRed(Node* p) { return (p == NULL) ? FALSE:p->color == RED; }

Node* rotateRight(Node* h) {
    Node* x = h->left;
    h->left = x->right;
    x->right = h;
    x->color = x->right->color;
    x->right->color = RED;
    x->size = h->size;
    h->size = sizeNode(h->left) + sizeNode(h->right) + 1;
    return x;
}

Node* rotateLeft(Node* h) {
    Node* x = h->right;
    h->right = x->left;
    x->left = h;
    x->color = x->left->color;
    x->left->color = RED;
    x->size = h->size;
    h->size = sizeNode(h->left) + sizeNode(h->right) + 1;
    return x;
}

void flipColors(Node* h) {
    h->color = !h->color;
    h->left->color = !h->left->color;
    h->right->color = !h->right->color;
}

Node* moveRedLeft(Node* h) {
    flipColors(h);
    if (isRed(h->right->left)) {
        h->right = rotateRight(h->right);
        h = rotateLeft(h);
        flipColors(h);
    }
    return h;
}

Node* moveRedRight(Node* h) {
    flipColors(h);
    if (isRed(h->left->left)) {
        h = rotateRight(h);
        flipColors(h);
    }
    return h;
}

Node* balance(Node* h) {
    if (isRed(h->right)) h = rotateLeft(h);
    if (isRed(h->left) && isRed(h->left->left)) h = rotateRight(h);
    if (isRed(h->left) && isRed(h->right)) flipColors(h);

    h->size = sizeNode(h->left) + sizeNode(h->right) + 1;
    return h;
}